var canvas,stage,zoom;
/* main hook*/
$(document).ready( function() {
	//$(document.body).bind( "keypress", function(event) { keyProc(event); } );
	init();
});

function init() {
	canvas= document.getElementById("demoCanvas");
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;   
	stage = new createjs.Stage("demoCanvas");

	drawShiningCity(stage);
	canvas.addEventListener("mousewheel", MouseWheelHandler, false);
	canvas.addEventListener("DOMMouseScroll", MouseWheelHandler, false);		
	function MouseWheelHandler(e) {
		if(Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))>0)
			zoom=1.1;
		else
			zoom=1/1.1;
		stage.regX += stage.mouseX - stage.regX;
		stage.regY += stage.mouseY - stage.regY;
		stage.x=stage.mouseX;
		stage.y=stage.mouseY;	
		stage.scaleX=stage.scaleY*=zoom;
		stage.update();
	}


	stage.addEventListener("stagemousedown", function(e) {
		var offset={x:stage.x-e.stageX,y:stage.y-e.stageY};
		stage.addEventListener("stagemousemove",function(ev) {
			stage.x = ev.stageX+offset.x;
			stage.y = ev.stageY+offset.y;
			stage.update();
		});
	
	stage.addEventListener("stagemouseup", function(){
		stage.removeAllEventListeners("stagemousemove");
	});
	}); 

	stage.update();
}